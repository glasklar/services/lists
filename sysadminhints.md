# Random factoids
- hyperkitty is the name of the list archive software
- postorious is the name of the django software providing the web interface
- /etc/mailman3/mailman.cfg (generated from Ansible template) is configuring mailman, including
  - db connection details
  - REST API config, like port (8001) and admin password
  - inclusion of /etc/mailman3/mailman-hyperkitty.cfg configuring hyperkitty

# A few useful resources
## mailman
- systemctl status mailman3
- /var/log/mailman3/mailman.log

## web
- systemctl status mailman3-web
- logs in /var/log/mailman3/web/mailman-web.log
- /etc/mailman3/uwsgi.ini is used by mailman3-web.service to start /usr/bin/uwsgi binding to /run/mailman3-web/uwsgi.sock which Apache proxies to (cf. /etc/apache2/sites-available/vhosts.conf)
- /etc/mailman3/mailman-web.py is imported, overriding `settings` (this is what the docs refer to as "djangos's settings.py", see /usr/share/mailman3-web/settings.py)
- mailman-web help
- Administer users in [Django administration](https://lists.sigsum.org/mailman3/admin/)
  - See https://git.glasklar.is/glasklar/services/lists/-/issues/22 for how to deal with a CSS bug with Firefox
- External documentation
  - [Django for Mailman Admins](https://docs.mailman3.org/en/latest/django-primer.html)
  - [Django documentation -- Settings](https://docs.djangoproject.com/en/3.2/ref/settings/)

### hyperkitty -- list archives
- https://docs.mailman3.org/_/downloads/hyperkitty/en/latest/pdf/
- /etc/mailman3/mailman-hyperkitty.cfg


# Changing default handling of non-members

Unknown sender addresses are added to the non-members list when received by Mailman.

The setting "Default action to take when a non-member posts to the
list", found under "Message Acceptance", determines how messages from
non-members are treated.

For public lists to which unknown senders should end up in the moderation queue set it to "Hold for moderation".
This results in list moderators receiving one email every 24h when the queue is not empty.

For lists where it is OK that non-members receive a rejection message when sending to the list, set it to "Reject".
Setting it to "Discard" instead should lower the risk of [backscatter](https://en.wikipedia.org/wiki/Backscatter_(email))
but will leave the sender without any indication that their message wasn't posted to the list.

We changed from "Reject" to "Discard" for sigsum-general@ on 2023-09-27. We later changed it to "Hold".

