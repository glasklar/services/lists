This repository collects user instructions and tips & tricks, and is used for tracking operational issues with https://lists.sigsum.org/.

# Resources

 - [Mailman FAQ](https://docs.mailman3.org/en/latest/faq.html)
 
# Allowing a certain sender to post to a list

You might want to add exceptions to the general rule that a sender has to be subscribed to a list in order to post to it. An example would be the git user on a gitolite machine, sending commit messages to a commit list.

Go to the configuration page of the list and select Users -> Non-Members.
Locate the sender and click Non-member Options for it.
At the end of the page there is a Moderation option. Select Default processing and save changes.
